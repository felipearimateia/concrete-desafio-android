package br.com.concretesolutions.githubjavapop.remote;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.Request;

/**
 * Created by felipets on 12/2/16.
 * http://stackoverflow.com/a/39273825/612354
 */

public class CachingInterceptor implements Interceptor {
    @Override
    public okhttp3.Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        request = new Request.Builder()
                .cacheControl(new CacheControl.Builder()
                        .maxAge(1, TimeUnit.DAYS)
                        .minFresh(4, TimeUnit.HOURS)
                        .maxStale(8, TimeUnit.HOURS)
                        .build())
                .url(request.url())
                .build();


        return chain.proceed(request);
    }
}
