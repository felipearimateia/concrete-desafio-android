package br.com.concretesolutions.githubjavapop.entity;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.util.List;

/**
 * Created by felipets on 12/1/16.
 */

@JsonObject
public class Result<T> {

    @JsonField(name = "total_count")
    private Long totalCount;

    @JsonField(name = "incomplete_results")
    private Boolean incompleteResults;

    @JsonField
    private List<T> items;

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }

    public Boolean getIncompleteResults() {
        return incompleteResults;
    }

    public void setIncompleteResults(Boolean incompleteResults) {
        this.incompleteResults = incompleteResults;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }
}
