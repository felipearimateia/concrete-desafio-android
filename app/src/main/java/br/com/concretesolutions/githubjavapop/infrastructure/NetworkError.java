package br.com.concretesolutions.githubjavapop.infrastructure;

import br.com.concretesolutions.githubjavapop.R;

/**
 * Created by felipets on 12/1/16.
 */

public class NetworkError extends Exception {
    private int code;

    public NetworkError(int code, String message) {
        super(message);
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public static int getMessage(int code) {

        switch (code) {
            case 404:
            case 422:
                return R.string.not_resul;
            default:
                return R.string.generic_error;
        }
    }
}
