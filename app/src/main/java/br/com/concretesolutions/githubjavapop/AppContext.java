package br.com.concretesolutions.githubjavapop;


import br.com.concretesolutions.githubjavapop.injection.AppComponent;
import br.com.concretesolutions.githubjavapop.injection.ViewComponent;

public class AppContext {

    private static AppComponent appComponent;
    private static ViewComponent viewComponent;

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    public static void setAppComponent(AppComponent appComponent) {
        AppContext.appComponent = appComponent;
    }

    public static ViewComponent getViewComponent() {
        return viewComponent;
    }

    public static void setViewComponent(ViewComponent viewComponent) {
        AppContext.viewComponent = viewComponent;
    }
}
