package br.com.concretesolutions.githubjavapop.repository;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

import javax.inject.Inject;

import br.com.concretesolutions.githubjavapop.entity.Pull;
import br.com.concretesolutions.githubjavapop.entity.Repo;
import br.com.concretesolutions.githubjavapop.entity.Result;
import br.com.concretesolutions.githubjavapop.remote.GithubAPI;
import br.com.concretesolutions.githubjavapop.infrastructure.NetworkError;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by felipets on 12/1/16.
 */

public class GithubRepository {

    private final GithubAPI githubAPI;

    @Inject
    public GithubRepository(GithubAPI githubAPI) {
        this.githubAPI = githubAPI;
    }

    public void searchRepo(String query, String sort, int page,
                           @NonNull OnResponseListener<Result<Repo>> listener) {

        Call<Result<Repo>> call = githubAPI.searchRepo(query, sort, page);

        call.enqueue(new Callback<Result<Repo>>() {
            @Override
            public void onResponse(Call<Result<Repo>> call, Response<Result<Repo>> response) {
                if (response.isSuccessful()) {
                    listener.onResponse(response.body(), null);
                }
                else {
                    int code = response.code();
                    String message = response.message();
                    listener.onResponse(null, new NetworkError(code, message));
                }
            }

            @Override
            public void onFailure(Call<Result<Repo>> call, Throwable t) {
                listener.onResponse(null, t);
            }
        });
    }

    public void getPulls(@NonNull String login, @NonNull String name, @NonNull OnResponseListener<List<Pull>> listener) {
        Call<List<Pull>> call = githubAPI.getPulls(login, name);
        call.enqueue(new Callback<List<Pull>>() {
            @Override
            public void onResponse(Call<List<Pull>> call, Response<List<Pull>> response) {
                if (response.isSuccessful()) {
                    listener.onResponse(response.body(), null);
                }
                else {
                    int code = response.code();
                    String message = response.message();
                    listener.onResponse(null, new NetworkError(code, message));
                }
            }

            @Override
            public void onFailure(Call<List<Pull>> call, Throwable t) {
                listener.onResponse(null, t);
            }
        });
    }


    public interface OnResponseListener<T> {
        void onResponse(@Nullable T response, @Nullable Throwable error);
    }
}
