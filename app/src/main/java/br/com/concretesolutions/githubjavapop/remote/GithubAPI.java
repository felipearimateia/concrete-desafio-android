package br.com.concretesolutions.githubjavapop.remote;

import java.util.List;

import br.com.concretesolutions.githubjavapop.entity.Pull;
import br.com.concretesolutions.githubjavapop.entity.Repo;
import br.com.concretesolutions.githubjavapop.entity.Result;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by felipets on 12/1/16.
 */

public interface GithubAPI {

    @GET("/search/repositories")
    Call<Result<Repo>> searchRepo(@Query("q") String query,
                                  @Query("sort") String sort,
                                  @Query("page") int page);

    @GET("repos/{login}/{name}/pulls")
    Call<List<Pull>> getPulls(@Path("login") String login,
                              @Path("name") String fullName);

}
