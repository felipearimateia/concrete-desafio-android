package br.com.concretesolutions.githubjavapop.business;

import android.support.annotation.Nullable;

/**
 * Created by felipearimateia on 06/09/16.
 */

public interface OnBusinessListener<T> {
    void onResult(@Nullable T result, @Nullable String error);
}
