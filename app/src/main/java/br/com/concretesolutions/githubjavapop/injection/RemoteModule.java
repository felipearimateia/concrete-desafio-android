package br.com.concretesolutions.githubjavapop.injection;

import br.com.concretesolutions.githubjavapop.remote.GithubAPI;
import br.com.concretesolutions.githubjavapop.remote.RemoteAdapter;
import dagger.Module;
import dagger.Provides;

/**
 * Created by felipets on 12/1/16.
 */

@Module
public class RemoteModule {

    @Provides
    public GithubAPI providerGithubAPI() {
        return RemoteAdapter.getApi(GithubAPI.class);
    }
}
