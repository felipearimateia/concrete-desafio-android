package br.com.concretesolutions.githubjavapop.remote;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.github.aurae.retrofit2.LoganSquareConverterFactory;

import java.io.File;
import java.util.concurrent.TimeUnit;

import br.com.concretesolutions.githubjavapop.BuildConfig;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.internal.cache.CacheInterceptor;
import okhttp3.internal.cache.InternalCache;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;

/**
 * Created by felipe.arimateia on 03/11/15.
 */
public class RemoteAdapter {

    private static Retrofit retrofit;

    private static final int CACHE_SIZE = 100 * 1024 * 1024; //200MB
    private static final int TIME_OUT = 10; //seconds;

    private RemoteAdapter() {
    }

    public static void init(@Nullable File cacheDir) {
        createAdapter(cacheDir);
    }

    public static synchronized <T>  T getApi(@NonNull Class<T> service) {
        return retrofit.create(service);
    }

    private static void createAdapter(@Nullable File cacheDir) {

        retrofit = new Retrofit.Builder()
                .client(createClient(cacheDir))
                .baseUrl("https://api.github.com")
                .addConverterFactory(LoganSquareConverterFactory.create())
                .build();
    }

    private static OkHttpClient createClient(File cacheDir) {

        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(logging);
        }

        if (cacheDir != null) {
            Cache cache = new Cache(cacheDir, CACHE_SIZE);
            builder.cache(cache);
            builder.addInterceptor(new CachingInterceptor());
        }

        builder.connectTimeout(TIME_OUT, TimeUnit.SECONDS);

        return builder.build();
    }
}
