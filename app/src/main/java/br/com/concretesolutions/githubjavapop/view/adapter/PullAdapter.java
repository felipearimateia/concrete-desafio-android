package br.com.concretesolutions.githubjavapop.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import br.com.concretesolutions.githubjavapop.R;
import br.com.concretesolutions.githubjavapop.entity.Owner;
import br.com.concretesolutions.githubjavapop.entity.Pull;
import br.com.concretesolutions.githubjavapop.entity.Repo;
import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.picasso.transformations.CropCircleTransformation;

/**
 * Created by felipets on 12/2/16.
 */

public class PullAdapter extends BaseAdapter<Pull, PullAdapter.ViewHolder>{


    public PullAdapter(Context context, List<Pull> itens) {
        super(context, itens);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(getContext())
                .inflate(R.layout.item_pull, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Pull pull = getItem(position);
        holder.pullTitle.setText(pull.getTitle());
        holder.pullBody.setText(pull.getBody());

        Owner owner = pull.getUser();
        holder.username.setText(owner.getLogin());

        Picasso.with(getContext()).load(owner.getAvatarUrl())
                .transform(new CropCircleTransformation())
                .into(holder.photoUser);

    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.pull_title)
        TextView pullTitle;

        @BindView(R.id.pull_body)
        TextView pullBody;

        @BindView(R.id.photo_user)
        ImageView photoUser;

        @BindView(R.id.username)
        TextView username;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener((view) -> {

                int position = getAdapterPosition();
                Pull pull = getItem(position);

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(pull.getUrl()));
                getContext().startActivity(intent);

            });
        }
    }
}
