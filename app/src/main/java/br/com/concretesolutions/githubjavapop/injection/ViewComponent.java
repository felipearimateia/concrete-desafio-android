package br.com.concretesolutions.githubjavapop.injection;


import br.com.concretesolutions.githubjavapop.injection.scope.PerActivity;
import br.com.concretesolutions.githubjavapop.view.MainActivity;
import br.com.concretesolutions.githubjavapop.view.RepoDetailActivity;
import dagger.Component;

/**
 * Created by felipearimateia on 05/09/16.
 */

@PerActivity
@Component(dependencies = {AppComponent.class})
public interface ViewComponent {
    void inject(MainActivity mainActivity);
    void inject(RepoDetailActivity repoDetailActivity);
}
