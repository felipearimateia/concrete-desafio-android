package br.com.concretesolutions.githubjavapop.view;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import br.com.concretesolutions.githubjavapop.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by felipets on 12/1/16.
 */

public class BaseActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    @Nullable
    Toolbar toolbar;

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
        initToolBar();
    }

    protected void initToolBar() {
        if (toolbar == null) {
            return;
        }

        setSupportActionBar(toolbar);
    }
}
