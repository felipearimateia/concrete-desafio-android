package br.com.concretesolutions.githubjavapop.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

/**
 * Created by felipets on 12/2/16.
 */

@JsonObject
public class Pull implements Parcelable {

    @JsonField
    private Long id;

    @JsonField
    private String title;

    @JsonField
    private String body;

    @JsonField
    private String state;

    @JsonField(name = "user")
    private Owner user;

    @JsonField(name = "html_url")
    private String url;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Owner getUser() {
        return user;
    }

    public void setUser(Owner user) {
        this.user = user;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Pull() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.title);
        dest.writeString(this.body);
        dest.writeString(this.state);
        dest.writeParcelable(this.user, flags);
        dest.writeString(this.url);
    }

    protected Pull(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.title = in.readString();
        this.body = in.readString();
        this.state = in.readString();
        this.user = in.readParcelable(Owner.class.getClassLoader());
        this.url = in.readString();
    }

    public static final Creator<Pull> CREATOR = new Creator<Pull>() {
        @Override
        public Pull createFromParcel(Parcel source) {
            return new Pull(source);
        }

        @Override
        public Pull[] newArray(int size) {
            return new Pull[size];
        }
    };
}
