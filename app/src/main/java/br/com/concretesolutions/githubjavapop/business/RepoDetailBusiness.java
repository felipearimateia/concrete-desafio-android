package br.com.concretesolutions.githubjavapop.business;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;

import br.com.concretesolutions.githubjavapop.JavaPopApplication;
import br.com.concretesolutions.githubjavapop.R;
import br.com.concretesolutions.githubjavapop.entity.Pull;
import br.com.concretesolutions.githubjavapop.entity.Repo;
import br.com.concretesolutions.githubjavapop.infrastructure.NetworkError;
import br.com.concretesolutions.githubjavapop.repository.GithubRepository;

/**
 * Created by felipets on 12/1/16.
 */

public class RepoDetailBusiness {

    private final GithubRepository repository;
    private final JavaPopApplication application;

    private int opened = 0;
    private int closed = 0;

    @Inject
    public RepoDetailBusiness(GithubRepository repository, JavaPopApplication application) {
        this.repository = repository;
        this.application = application;
    }

    public void loadPulls(String login, String name,
                         @NonNull OnBusinessListener<List<Pull>> listener) {

        repository.getPulls(login, name, (pulls, error) -> {
            if (error != null) {
                listener.onResult(null, parseError(error));
                return;
            }

            calculatorPulls(pulls);

            listener.onResult(pulls, null);
        });
    }

    public int getOpened() {
        return opened;
    }

    public int getClosed() {
        return closed;
    }

    private void calculatorPulls(List<Pull> pulls) {

        for (Pull pull : pulls) {

            if (pull.getState().equals("open")) {
                opened++;
            }
            else if (pull.getState().equals("closed")){
                closed++;
            }

        }
    }

    private String parseError(Throwable error) {

        if (error instanceof NetworkError) {
            int code = ((NetworkError)error).getCode();
            return application.getString(NetworkError.getMessage(code));
        }

        return application.getString(R.string.generic_error);
    }
}
