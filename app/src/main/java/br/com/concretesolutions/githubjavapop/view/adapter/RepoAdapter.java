package br.com.concretesolutions.githubjavapop.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.concretesolutions.githubjavapop.R;
import br.com.concretesolutions.githubjavapop.entity.Owner;
import br.com.concretesolutions.githubjavapop.entity.Repo;
import br.com.concretesolutions.githubjavapop.view.RepoDetailActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.picasso.transformations.CropCircleTransformation;

/**
 * Created by felipets on 12/2/16.
 */

public class RepoAdapter extends BaseAdapter<Repo, RepoAdapter.ViewHolder>{


    public RepoAdapter(Context context, List<Repo> itens) {
        super(context, itens);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(getContext())
                .inflate(R.layout.item_repo, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Repo repo = getItem(position);
        holder.repoName.setText(repo.getName());
        holder.repoDescription.setText(repo.getDescription());
        holder.countFork.setText(String.valueOf(repo.getForksCount()));
        holder.countStars.setText(String.valueOf(repo.getStargazersCount()));

        Owner owner = repo.getOwner();
        holder.username.setText(owner.getLogin());

        Picasso.with(getContext()).load(owner.getAvatarUrl())
                .transform(new CropCircleTransformation())
                .into(holder.photoUser);

    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name_repo)
        TextView repoName;

        @BindView(R.id.repo_description)
        TextView repoDescription;

        @BindView(R.id.photo_user)
        ImageView photoUser;

        @BindView(R.id.username)
        TextView username;

        @BindView(R.id.countFork)
        AppCompatTextView countFork;

        @BindView(R.id.countStars)
        AppCompatTextView countStars;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener((view) -> {
                int position = getAdapterPosition();
                Repo repo = getItem(position);

                Intent intent = new Intent(getContext(), RepoDetailActivity.class);
                intent.putExtra(RepoDetailActivity.EXTRA_LOGIN, repo.getOwner().getLogin());
                intent.putExtra(RepoDetailActivity.EXTRA_NAME, repo.getName());
                getContext().startActivity(intent);

            });
        }
    }
}
