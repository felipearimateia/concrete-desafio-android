package br.com.concretesolutions.githubjavapop.injection;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.io.File;

import br.com.concretesolutions.githubjavapop.JavaPopApplication;
import br.com.concretesolutions.githubjavapop.remote.RemoteAdapter;
import dagger.Module;
import dagger.Provides;

/**
 * Created by felipets on 12/1/16.
 */

@Module
public class AppModule {

    private final JavaPopApplication application;

    public AppModule(JavaPopApplication application) {
        this.application = application;
        RemoteAdapter.init(new File(application.getCacheDir(), "http"));
    }

    @Provides
    public JavaPopApplication provideRanguinhoApplication() {
        return this.application;
    }

    @Provides
    SharedPreferences providesSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(application);
    }
}
