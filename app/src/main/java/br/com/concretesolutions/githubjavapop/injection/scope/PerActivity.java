package br.com.concretesolutions.githubjavapop.injection.scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by felipearimateia on 11/9/16.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerActivity {}
