package br.com.concretesolutions.githubjavapop.business;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;

import br.com.concretesolutions.githubjavapop.JavaPopApplication;
import br.com.concretesolutions.githubjavapop.R;
import br.com.concretesolutions.githubjavapop.entity.Repo;
import br.com.concretesolutions.githubjavapop.infrastructure.NetworkError;
import br.com.concretesolutions.githubjavapop.repository.GithubRepository;

/**
 * Created by felipets on 12/1/16.
 */

public class MainBusiness {

    private final GithubRepository repository;
    private final JavaPopApplication application;

    @Inject
    public MainBusiness(GithubRepository repository, JavaPopApplication application) {
        this.repository = repository;
        this.application = application;
    }

    public void loadRepo(String language, int page,
                         @NonNull OnBusinessListener<List<Repo>> listener) {
        String query = "language:" + language;

        repository.searchRepo(query, "stars", page, (response, error) -> {

            if (error != null) {
                listener.onResult(null, parseError(error));
                return;
            }

            listener.onResult(response.getItems(), null);

        });
    }


    private String parseError(Throwable error) {

        if (error instanceof NetworkError) {
            int code = ((NetworkError)error).getCode();
            return application.getString(NetworkError.getMessage(code));
        }

        return application.getString(R.string.generic_error);
    }
}
