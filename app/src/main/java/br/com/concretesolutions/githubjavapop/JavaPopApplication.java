package br.com.concretesolutions.githubjavapop;

import android.app.Application;

import br.com.concretesolutions.githubjavapop.injection.AppComponent;
import br.com.concretesolutions.githubjavapop.injection.AppModule;
import br.com.concretesolutions.githubjavapop.injection.DaggerAppComponent;
import br.com.concretesolutions.githubjavapop.injection.DaggerViewComponent;
import br.com.concretesolutions.githubjavapop.injection.ViewComponent;

/**
 * Created by felipets on 12/1/16.
 */

public class JavaPopApplication extends Application {

    AppComponent appComponent;
    ViewComponent viewComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();

        viewComponent = DaggerViewComponent.builder()
                .appComponent(appComponent)
                .build();

        AppContext.setAppComponent(appComponent);
        AppContext.setViewComponent(viewComponent);
    }
}
