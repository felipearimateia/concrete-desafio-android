package br.com.concretesolutions.githubjavapop.injection;

import javax.inject.Singleton;

import br.com.concretesolutions.githubjavapop.JavaPopApplication;
import br.com.concretesolutions.githubjavapop.remote.GithubAPI;
import dagger.Component;

/**
 * Created by felipets on 12/1/16.
 */

@Singleton
@Component(modules = {AppModule.class, RemoteModule.class, RepositoryModule.class})
public interface AppComponent {

    JavaPopApplication javaPopApplication();
    GithubAPI githubApi();
}
