package br.com.concretesolutions.githubjavapop.injection;

import br.com.concretesolutions.githubjavapop.remote.GithubAPI;
import br.com.concretesolutions.githubjavapop.repository.GithubRepository;
import dagger.Module;
import dagger.Provides;

/**
 * Created by felipets on 12/1/16.
 */

@Module
public class RepositoryModule {

    @Provides
    public GithubRepository providerGithubRepository(GithubAPI githubAPI) {
        return new GithubRepository(githubAPI);
    }
}
