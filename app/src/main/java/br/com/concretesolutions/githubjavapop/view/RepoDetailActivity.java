package br.com.concretesolutions.githubjavapop.view;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.SpannedString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import javax.inject.Inject;

import br.com.concretesolutions.githubjavapop.AppContext;
import br.com.concretesolutions.githubjavapop.R;
import br.com.concretesolutions.githubjavapop.business.RepoDetailBusiness;
import br.com.concretesolutions.githubjavapop.view.adapter.PullAdapter;
import butterknife.BindView;

public class RepoDetailActivity extends BaseActivity {


    public static final String EXTRA_LOGIN = "login";
    public static final String EXTRA_NAME = "name";

    @Inject
    RepoDetailBusiness business;

    @BindView(R.id.pull_total)
    TextView pullTotal;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private PullAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppContext.getViewComponent().inject(this);
        setContentView(R.layout.activity_repo_detail);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new PullAdapter(this, new ArrayList<>());
        recyclerView.setAdapter(adapter);

        String login = getIntent().getStringExtra(EXTRA_LOGIN);
        String name = getIntent().getStringExtra(EXTRA_NAME);

        setTitle(name);

        loadData(login, name);
    }

    private void loadData(String login, String name) {

        business.loadPulls(login, name, (result, error) -> {

            if (!TextUtils.isEmpty(error)) {
                Snackbar.make(recyclerView,error,
                        Snackbar.LENGTH_SHORT).show();
                return;
            }

            adapter.addAll(result);
            updateTotal();
        });
    }

    @Override
    protected void initToolBar() {
        super.initToolBar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    private void updateTotal() {

        int gold = ContextCompat.getColor(this, R.color.harvest_gold);
        int ebony = ContextCompat.getColor(this, R.color.ebony_clay);

        SpannableString opened = new SpannableString(business.getOpened() + " opened");
        opened.setSpan(new ForegroundColorSpan(gold), 0, opened.length(), 0);


        SpannableString closed = new SpannableString(business.getClosed() + " closed");
        closed.setSpan(new ForegroundColorSpan(ebony), 0, closed.length(), 0);

        pullTotal.setText(opened);
        pullTotal.append(" / ");
        pullTotal.append(closed);
    }
}
