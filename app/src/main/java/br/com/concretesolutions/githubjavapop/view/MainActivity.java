package br.com.concretesolutions.githubjavapop.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.com.concretesolutions.githubjavapop.AppContext;
import br.com.concretesolutions.githubjavapop.R;
import br.com.concretesolutions.githubjavapop.business.MainBusiness;
import br.com.concretesolutions.githubjavapop.business.OnBusinessListener;
import br.com.concretesolutions.githubjavapop.entity.Repo;
import br.com.concretesolutions.githubjavapop.view.adapter.RepoAdapter;
import butterknife.BindView;

public class MainActivity extends BaseActivity implements OnBusinessListener<List<Repo>> {

    @Inject
    MainBusiness mainBusiness;

    @BindView(R.id.content_main)
    RecyclerView contentMain;

    private RepoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppContext.getViewComponent().inject(this);

        setContentView(R.layout.activity_main);

        initRecyclerView();

        mainBusiness.loadRepo("java", 0, this);
    }

    private void initRecyclerView() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        contentMain.setLayoutManager(layoutManager);

        contentMain.addOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {

            @Override
            public void onLoadMore(int currentPage) {
                mainBusiness.loadRepo("java", currentPage, MainActivity.this);
            }
        });

        adapter = new RepoAdapter(this, new ArrayList<>());
        contentMain.setAdapter(adapter);
    }

    @Override
    public void onResult(@Nullable List<Repo> result, @Nullable String error) {

        if (!TextUtils.isEmpty(error)) {
            Snackbar.make(contentMain,error,
                    Snackbar.LENGTH_SHORT).show();
            return;
        }

        adapter.addAll(result);
    }
}
