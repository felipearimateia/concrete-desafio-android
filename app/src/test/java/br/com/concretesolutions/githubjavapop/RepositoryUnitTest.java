package br.com.concretesolutions.githubjavapop;


import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

import br.com.concretesolutions.githubjavapop.entity.Pull;
import br.com.concretesolutions.githubjavapop.entity.Repo;
import br.com.concretesolutions.githubjavapop.entity.Result;
import br.com.concretesolutions.githubjavapop.remote.GithubAPI;
import br.com.concretesolutions.githubjavapop.remote.RemoteAdapter;
import br.com.concretesolutions.githubjavapop.infrastructure.NetworkError;
import br.com.concretesolutions.githubjavapop.repository.GithubRepository;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by felipets on 12/1/16.
 */

public class RepositoryUnitTest {

    @Before
    public void initRemote() {
        RemoteAdapter.init(null);
    }

    @Test
    public void testSearch200() throws InterruptedException {

        String query ="language:Java";
        String sort = "stars";
        int page = 0;

        GithubAPI githubAPI = RemoteAdapter.getApi(GithubAPI.class);
        GithubRepository repository = new GithubRepository(githubAPI);

        final AtomicReference<Result<Repo>> reference = new AtomicReference<>();
        final CountDownLatch latch = new CountDownLatch(1);

        repository.searchRepo(query, sort, page, (response, error) -> {
            reference.set(response);
            latch.countDown();
        });

        assertTrue(latch.await(10, SECONDS));

        Result<Repo> result = reference.get();
        assertNotNull(result);
        assertTrue(result.getItems().size() > 0);
    }

    @Test
    public void testSearch422() throws InterruptedException {

        String query ="language:vazio";
        String sort = "stars";
        int page = 0;

        GithubAPI githubAPI = RemoteAdapter.getApi(GithubAPI.class);
        GithubRepository repository = new GithubRepository(githubAPI);

        final AtomicReference<Throwable> reference = new AtomicReference<>();
        final CountDownLatch latch = new CountDownLatch(1);

        repository.searchRepo(query, sort, page, (response, error) -> {
            reference.set(error);
            latch.countDown();
        });

        assertTrue(latch.await(10, SECONDS));

        NetworkError error = (NetworkError)reference.get();
        assertTrue(error.getCode() == 422);
    }

    @Test
    public void testPull200() throws  InterruptedException {

        GithubAPI githubAPI = RemoteAdapter.getApi(GithubAPI.class);
        GithubRepository repository = new GithubRepository(githubAPI);

        final AtomicReference<List<Pull>> reference = new AtomicReference<>();
        final CountDownLatch latch = new CountDownLatch(1);

        repository.getPulls("ReactiveX", "RxJava", (pulls, error) -> {
            reference.set(pulls);
            latch.countDown();
        });


        assertTrue(latch.await(10, SECONDS));

       List<Pull> result = reference.get();
        assertNotNull(result);
        assertTrue(result.size() > 0);
    }
}
